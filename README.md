# syndicate-pty-driver

[protocol]: protocols/schemas/pty.prs

A PTY (pseudoterminal) driver for [Syndicate](https://syndicate-lang.org), plus [protocol
schema][protocol] for PTY interaction via Syndicate.

Expects to be started from
[syndicate-server](https://git.syndicate-lang.org/syndicate-lang/syndicate-rs/), perhaps via
configuration like this:

```preserves
<require-service <daemon pty>>
<daemon pty { protocol: application/syndicate, argv: "syndicate-pty-driver" }>
```

It publishes a service object which then speaks the [PTY protocol][protocol]. You can start a
program running in a fresh PTY like this:

```preserves
# As soon as the PTY driver publishes its service object, $cap, ...
? <service-object <daemon pty> ?cap> [
  # ... assert interest in existence of a session with id 1 running `bash -i`.
  $cap += <pty-session 1 [bash -i]>
]
```

Then, expect `<pty-output ...>` messages and send `<pty-input ...>` messages to interact with
the subprocess.

See the [protocol definition][protocol] for more about interacting with PTYs over Syndicate.
