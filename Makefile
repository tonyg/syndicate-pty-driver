all:

clean:
	rm -rf target

ci-release: x86_64-binary-release aarch64-binary-release
	rm -rf target/dist
	for arch in x86_64 aarch64; do \
		mkdir -p target/dist/$$arch; \
		cp -a target/target.$$arch/$$arch-unknown-linux-musl*/release/syndicate-pty-driver target/dist/$$arch; \
	done

%-binary: %-binary-release

%-binary-release:
	CARGO_TARGET_DIR=target/target.$* cross build --target $*-unknown-linux-musl --release --all-targets

%-binary-debug:
	CARGO_TARGET_DIR=target/target.$* cross build --target $*-unknown-linux-musl --all-targets
